FROM python:3-alpine

# Base tools
RUN apk add --no-cache curl jq bash gawk grep coreutils

# Unix bridge version
RUN pip install programaker-unix-bridge==0.0.1.post8

# Install configuration and set config path
ADD configuration /config
ENV CONFIG_PATH=/config

# Set launch command
CMD programaker-unix-bridge
