#!/bin/sh

set -eu

cd gcnc-galicia-covid19-new-cases

EXPIRATION_TIME=$(( 60 * 4 )) # 4 Hours

# Check if there's data more recent than EXPIRATION_TIME
PULL=0
if [ ! -f data.json ];then
    PULL=1
else
    FILE_TIMESTAMP=`stat data.json --printf="%Y"`
    CLOCK_TIMESTAMP=`date +%s`

    if [ $(( $FILE_TIMESTAMP + $EXPIRATION_TIME )) -lt $CLOCK_TIMESTAMP ];then
        PULL=1
    fi
fi

# Pull data if needed
if [ $PULL -eq 1 ];then
    bash ./galicia-covid19-new-cases.sh -a | jq -c > data.json
fi

cat data.json
