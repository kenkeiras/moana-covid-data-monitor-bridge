#!/bin/sh

set -eu

bash get-covid-restriction-all-data.sh | jq ".town[] | select (.id==\"$1\")"
