#!/bin/sh

set -eu

export JSON_GALICIA=`bash get-covid-restriction-all-data.sh`

cd gcr-galicia-covid19-restrictions
bash ./galicia-covid19-restrictions.sh --message-by-town-id=$1
