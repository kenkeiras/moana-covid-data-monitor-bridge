#!/bin/sh

set -eu

export JSON_GALICIA=`bash get-town-all-data.sh`

cd gcnc-galicia-covid19-new-cases

data=`bash ./galicia-covid19-new-cases.sh --message-by-town-id=$1`

echo "$data"
