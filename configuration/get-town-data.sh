#!/bin/sh

set -eu

bash get-town-all-data.sh | jq ".town[] | select (.id==\"$1\")"
